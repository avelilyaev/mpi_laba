﻿using System;
using System.Collections.Generic;
using System.IO;
using MPI;

namespace Scrambler
{
	class Program
	{
		static void Main(string[] args)
		{
			using (new MPI.Environment(ref args))
			{
				Intracommunicator comm = Communicator.world;

				if (comm.Rank == 0)
				{
					//Заполняем массив текстом
					ReadTextFromFile();

					//распределяем задачи между подчиненными процессами
					if (OriginalText.Count == comm.Size)
					{
						for (int i = 1; i < comm.Size; i++)
						{
							comm.Send(OriginalText[i], i, 0);
						}	
					}

					//Мастер берет на себя первую строку
					comm.Gather(EncryptLine(OriginalText[0]), 0, ref _outSrings);
					comm.Barrier();

					//выводим зашифрованный текст
					for (int i = 0; i < OriginalText.Count; i++)
					{
						Console.WriteLine(_outSrings[i]);
						File.AppendAllLines("result.txt", new string[] { _outSrings[i] });
					}
					Console.ReadLine();
				}
				else
				{
					String line = String.Empty;

					//Получаем нашу строку от мастера
					comm.Receive(0, 0, out line);

					comm.Gather(EncryptLine(line), 0, ref _outSrings);
					comm.Barrier();
				}
			}
		}

		/// <summary>
		/// Заполняем массив Strigs строками из текстового файла
		/// </summary>
		static void ReadTextFromFile()
		{
			OriginalText = new List<String>();
			try
			{
				using (StreamReader sr = new StreamReader("original text.txt"))
				{
					while (!sr.EndOfStream)
					{
						OriginalText.Add(sr.ReadLine());
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("The file could not be read:");
				Console.WriteLine(e.Message);
			}
		}

		/// <summary>
		/// Метод, шифрующий строку
		/// </summary>
		static String EncryptLine(String line)
		{
			String result = String.Empty;
			for (int i = 0; i < line.Length; i++)
			{
				switch (line[i])
				{
					case 'a':
						result += 's';
						break;
					case 'b':
						result += 'p';
						break;
					case 'c':
						result += 'x';
						break;
					case 'd':
						result += 'l';
						break;
					case 'e':
						result += 'r';
						break;
					case 'f':
						result += 'z';
						break;
					case 'g':
						result += 'i';
						break;
					case 'h':
						result += 'm';
						break;
					case 'i':
						result += 'a';
						break;
					case 'j':
						result += 'y';
						break;
					case 'k':
						result += 'e';
						break;
					case 'l':
						result += 'd';
						break;
					case 'm':
						result += 'w';
						break;
					case 'n':
						result += 't';
						break;
					case 'o':
						result += 'b';
						break;
					case 'p':
						result += 'g';
						break;
					case 'q':
						result += 'v';
						break;
					case 'r':
						result += 'n';
						break;
					case 's':
						result += 'j';
						break;
					case 't':
						result += 'o';
						break;
					case 'u':
						result += 'c';
						break;
					case 'v':
						result += 'f';
						break;
					case 'w':
						result += 'h';
						break;
					case 'x':
						result += 'q';
						break;
					case 'y':
						result += 'u';
						break;
					case 'z':
						result += 'k';
						break;
					default:
						result += line[i];
						break;
				}
			}
			return result;
		}

		/// <summary>
		/// Список, содержащий строки входного текста
		/// </summary>
		static List<String> OriginalText { get; set; }

		/// <summary>
		/// Массив, который будет содержать зашифрованный текст
		/// </summary>
		static String[] _outSrings;
	}
}
